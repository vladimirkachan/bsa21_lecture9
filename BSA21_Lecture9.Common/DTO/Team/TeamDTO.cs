﻿using System;

namespace BSA21_Lecture9.Common.DTO.Team
{
    public class TeamDTO : BaseDTO
    {
        public string Name {get; set;}
        public virtual DateTime CreatedAt {get; set;} 
    }
}
