﻿using System;
using System.Text.Json.Serialization;

namespace BSA21_Lecture9.Common.DTO.Task
{
    public class TaskCreateDTO : TaskDTO
    {
        [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore] public override DateTime CreatedAt { get => base.CreatedAt; set => base.CreatedAt = value; }
        [JsonIgnore] public override int State { get => base.State; set => base.State = value; }
    }
}
