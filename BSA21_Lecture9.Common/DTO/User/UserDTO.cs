﻿using System;

namespace BSA21_Lecture9.Common.DTO.User
{
    public class UserDTO : BaseDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email {get; set;}
        public virtual DateTime RegisteredAt {get; set;}
        public DateTime BirthDay {get; set;}
    }
}
