﻿using System;
using System.Text.Json.Serialization;

namespace BSA21_Lecture9.Common.DTO.User
{
    public class UserCreateDTO : UserDTO
    {
        [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore] public override DateTime RegisteredAt { get => base.RegisteredAt; set => base.RegisteredAt = value; }
    }
}
