﻿using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;

namespace BSA21_Lecture9.Common.DTO.Custom
{
    public class CustomProjectDTO
    {
        public ProjectDTO Project {get; set;}
        public TaskDTO LongestDescriptionTask {get; set;}
        public TaskDTO ShortestNameTask {get; set;}
        public int TotalCountOfTeamUsers {get; set;}
    }
}
