﻿using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.Common.DTO.User;

namespace BSA21_Lecture9.Common.DTO.Custom
{
    public class CustomUserDTO
    {
        public UserDTO User {get; set;}
        public ProjectDTO LastProject {get; set;}
        public int? TotalTaskUnderLastProject {get; set;}
        public int? TotalUnperformedTask { get; set;}
        public TaskDTO LongestTask {get; set;}
    }
}
