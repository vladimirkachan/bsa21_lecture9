﻿using System;

namespace BSA21_Lecture9.Common.DTO.Project
{
    public class ProjectDTO : BaseDTO
    {
        public string Name {get; set;}
        public string Description {get; set;}
        public DateTime Deadline {get; set;}
        public virtual DateTime CreatedAt {get; set;}
    }
}
