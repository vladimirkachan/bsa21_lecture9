﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture9.Common.DTO;
using BSA21_Lecture9.Common.DTO.Custom;
using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.Common.DTO.Team;
using BSA21_Lecture9.Common.DTO.User;
using Newtonsoft.Json;

namespace BSA21_Lecture9.Common
{
    public static class EndPoints
    {
        static readonly HttpClient client = new();
        const string host = @"https://localhost:44369/api/";

        static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }
        static async Task<string> GetEndPoint(string url)
        {
            var response = await client.GetAsync(url);
            return await GetResponseBody(response);
        }
        static async Task<string> PutEndPoint(string url, int id)
        {
            var content = new StringContent(JsonConvert.SerializeObject(id), Encoding.UTF8, "application/json");
            var response = await client.PutAsync(url, content);
            return await GetResponseBody(response);
        }
        static async Task<string> PutEndPoint(string url, BaseDTO dto)
        {
            var content = new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json");
            var response = await client.PutAsync(url, content);
            return await GetResponseBody(response);
        }
        static async Task<string> DeleteEndPoint(string url)
        {
            var response = await client.DeleteAsync(url);
            return await GetResponseBody(response);
        }
        static async Task<string> PostEndPoint(string url, BaseDTO dto)
        {
            var content = new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, content);
            return await GetResponseBody(response);
        }
        public static async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            var s = await GetEndPoint(host + "projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(s);
        }
        public static async Task<ProjectDTO> GetProject(int id)
        {
            var s = await GetEndPoint(host + $"projects/{id}");
            return JsonConvert.DeserializeObject<ProjectDTO>(s);
        }
        public static async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            var s = await GetEndPoint(host + "tasks");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<TaskDTO> GetTask(int id)
        {
            var s = await GetEndPoint(host + $"tasks/{id}");
            return JsonConvert.DeserializeObject<TaskDTO>(s);
        }
        public static async Task<IEnumerable<UserDTO>> GetUsers()
        {
            var s = await GetEndPoint(host + "users");
            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(s);
        }
        public static async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            var s = await GetEndPoint(host + "teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamDTO>>(s);
        }
        public static async Task<int> TaskCount(int projectId)
        {
            var count = await GetEndPoint(host + $"projects/{projectId}/taskCount");
            return Convert.ToInt32(count);
        }
        public static async Task<IEnumerable<TaskDTO>> GetTasksOnPerformer(int performerId, int maxLength = 45)
        {
            var s = await GetEndPoint(host + $"tasks/performer/{performerId}/{maxLength}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<IEnumerable<TaskDTO>> GetFinishedTask(int performerId, int year)
        {
            var s = await GetEndPoint(host + $"tasks/performer/{performerId}/year/{year}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>> GetTeamUsers(int minAge, int maxAge)
        {
            var s = await GetEndPoint(host + $"teams/withUsers/{minAge}/{maxAge}");
            return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>>(s);
        }
        public static async Task<IEnumerable<KeyValuePair<ProjectDTO, IEnumerable<TaskDTO>>>> GetProjectsWithTasks()
        {
            var s = await GetEndPoint(host + "projects/withTasks");
            return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<ProjectDTO, IEnumerable<TaskDTO>>>>(s);
        }
        public static async Task<IEnumerable<TaskDTO>> GetTasksByProject(int projectId)
        {
            var s = await GetEndPoint(host + $"tasks/projectId/{projectId}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<IEnumerable<KeyValuePair<UserDTO, IEnumerable<TaskDTO>>>> GetSortedUsers()
        {
            var s = await GetEndPoint(host + "users/sortedByNameAndTaskNameLength");
            return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<UserDTO, IEnumerable<TaskDTO>>>>(s);
        }
        public static async Task<CustomUserDTO> GetUserObject(int userId)
        {
            var s = await GetEndPoint(host + $"users/custom/{userId}");
            return JsonConvert.DeserializeObject<CustomUserDTO>(s);
        }
        public static async Task<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>> GetTeamsAndUsers()
        {
            var s = await GetEndPoint(host + "teams/andUsers");
            return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>>(s);
        }
        public static async Task<CustomProjectDTO> GetCustomProject(int projectId)
        {
            var s = await GetEndPoint(host + $"projects/custom/{projectId}");
            return JsonConvert.DeserializeObject<CustomProjectDTO>(s);
        }
        public static async Task<IEnumerable<TaskDTO>> GetUnfinishedTasks()
        {
            var s = await GetEndPoint(host + "tasks/unfinished");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<IEnumerable<TaskDTO>> GetFinishedTasks()
        {
            var s = await GetEndPoint(host + "tasks/finished");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(s);
        }
        public static async Task<int> TryFinishTask(int id)
        {
            var s = await PutEndPoint(host + "tasks/tryFinish", id);
            return JsonConvert.DeserializeObject<int>(s);
        }
        public static async Task<TeamDTO> AddTeam(TeamCreateDTO dto)
        {
            var s = await PostEndPoint(host + "teams", dto);
            return JsonConvert.DeserializeObject<TeamDTO>(s);
        }
        public static async Task<TeamDTO> UpdateTeam(int id, TeamCreateDTO dto)
        {
            var s = await PutEndPoint(host + $"teams/{id}", dto);
            return JsonConvert.DeserializeObject<TeamDTO>(s);
        }
        public static async Task DeleteTeam(int id)
        {
            await DeleteEndPoint(host + $"teams/{id}");
        }
        public static async Task AddUser(UserCreateDTO dto)
        {
            var s = await PostEndPoint(host + "users", dto);
        }
    }
}
