import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { BaseEntity } from "../models/base-entity";

@Injectable()
export class ModelService<T extends BaseEntity, N>
{
  constructor(private http: HttpClient, private url: string) { }

  get() {
    return this.http.get(this.url);
  }
  create(item: N)
  {
    return this.http.post(this.url, item);
  }
  update(item: T)
  {
    return this.http.put(`${this.url}/${item.id}`, item);
  }
  delete(id: number)
  {
    return this.http.delete(`${this.url}/${id}`);
  }

}
