import { BaseEntity } from '../base-entity';

export interface Team extends BaseEntity
{
  name: string;
  createdAt: Date;
}
