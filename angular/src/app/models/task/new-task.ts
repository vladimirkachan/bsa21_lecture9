import { State } from "./state";

export interface NewTask
{
  name: string;
  description: string;
  finishedAt: Date;
}
