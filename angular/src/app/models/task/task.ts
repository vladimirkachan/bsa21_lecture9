import { BaseEntity } from '../base-entity';
import { Project } from "../project/project";
import { User } from '../user/user';
import { State } from './state';

export interface Task extends BaseEntity
{
  project: Project;
  performer: User;
  name: string;
  description: string;
  state: State;
  createdAt: Date;
  finishedAt: Date;
}
