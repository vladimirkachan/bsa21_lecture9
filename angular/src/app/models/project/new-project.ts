export interface NewProject
{
  name: string;
  description: string;
  deadline: Date;
}
