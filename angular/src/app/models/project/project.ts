import { BaseEntity } from '../base-entity';
import { User } from '../user/user';
import { Team } from '../team/team';

export interface Project extends BaseEntity
{
  author: User;
  team: Team;
  name: string;
  description: string;
  deadline: Date;
  createdAt: Date;
}
