export interface NewUser
{
  firstName: string;
  lastName: string;
  email: string;
  birthDay: Date;
}
