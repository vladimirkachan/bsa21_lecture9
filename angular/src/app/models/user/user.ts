import { BaseEntity } from '../base-entity';
import { Team } from '../team/team';

export interface User extends BaseEntity
{
  team: Team;
  firstName: string;
  lastName: string;
  email: string;
  registeredAt: Date;
  birthDay: Date;
}
