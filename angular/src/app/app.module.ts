import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TeamListComponent } from './team-list/team-list.component';
import { UserListComponent } from './user-list/user-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectListComponent,
    TaskListComponent,
    TeamListComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule
    , RouterModule.forRoot([
      { path: "project-list", component: ProjectListComponent }
      , { path: "task-list", component: TaskListComponent }
      , { path: "team-list", component: TeamListComponent }
      , {path: "user-list", component: UserListComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
