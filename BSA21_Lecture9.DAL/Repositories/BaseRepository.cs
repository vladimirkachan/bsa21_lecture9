﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BSA21_Lecture9.DAL.Context;

namespace BSA21_Lecture9.DAL.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DataContext db;

        protected BaseRepository(DataContext context)
        {
            db = context;
        }

        public abstract Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null);
        public abstract Task<TEntity> Get(int id);
        public abstract Task<TEntity> Create(TEntity input);
        public abstract Task<TEntity> Update(TEntity input);
        public abstract Task<bool> Delete(int id);
    }
}
