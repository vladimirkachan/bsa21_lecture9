﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BSA21_Lecture9.DAL.Context;
using BSA21_Lecture9.DAL.Entities;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture9.DAL.Repositories
{
    public sealed class TeamRepository : BaseRepository<Team>
    {
        public TeamRepository(DataContext context) : base(context) { }

        public override async Task<IEnumerable<Team>> Get(Expression<Func<Team, bool>> filter = null)
        {
            if (filter == null) return await Task.Run(db.Teams.AsEnumerable);
            var f = filter.Compile();
            var query = from i in db.Teams.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override async Task<Team> Get(int id)
        {
            return await db.Teams.FindAsync(id);
        }
        public override async Task<Team> Create(Team input)
        {
            var value = await db.Teams.AddAsync(input);
            await db.SaveChangesAsync();
            return value.Entity;
        }
        public override async Task<Team> Update(Team input)
        {
            var item = await Get(input.Id);
            if (item == null) return null;
            item.Name = input.Name;
            await db.SaveChangesAsync();
            return item;
        }
        public override async Task<bool> Delete(int id)
        {
            var item = await Get(id);
            if (item == null) return false;
            db.Teams.Remove(item);
            await db.SaveChangesAsync();
            return true;
        }

    }
}
