﻿using BSA21_Lecture9.DAL.Context;
using BSA21_Lecture9.DAL.Entities;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public IRepository<Project> Projects {get;} 
        public IRepository<Task> Tasks {get;}
        public IRepository<User> Users {get;} 
        public IRepository<Team> Teams {get;}

        public UnitOfWork(DataContext context)
        {
            Projects = new ProjectRepository(context);
            Tasks = new TaskRepository(context);
            Users = new UserRepository(context);
            Teams = new TeamRepository(context);
        }
    }
}
