﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BSA21_Lecture9.DAL.Context;
using BSA21_Lecture9.DAL.Entities;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture9.DAL.Repositories
{
    public sealed class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(DataContext context) : base(context) {}
        public override async Task<IEnumerable<Project>> Get(Expression<Func<Project, bool>> filter = null)
        {
            if (filter == null) return await Task.Run(db.Projects.AsEnumerable);
            var f = filter.Compile();
            var query = from i in db.Projects.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override async Task<Project> Get(int id)
        {
            return await db.Projects.FindAsync(id);
        }
        public override async Task<Project> Create(Project input)
        {
            var value = await db.Projects.AddAsync(input);
            await db.SaveChangesAsync();
            return value.Entity;
        }
        public override async Task<Project> Update(Project input)
        {
            var item = await Get(input.Id);
            if (item == null) return null;
            item.Deadline = input.Deadline;
            item.Description = input.Description;
            item.Name = input.Name;
            await db.SaveChangesAsync();
            return item;
        }
        public override async Task<bool> Delete(int id)
        {
            var item = await Get(id);
            if (item == null) return false;
            db.Projects.Remove(item);
            await db.SaveChangesAsync();
            return true;
        }
    }
}
