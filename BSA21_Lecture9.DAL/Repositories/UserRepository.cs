﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BSA21_Lecture9.DAL.Context;
using BSA21_Lecture9.DAL.Entities;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture9.DAL.Repositories
{
    public sealed class UserRepository : BaseRepository<User>
    {
        public UserRepository(DataContext context) : base(context) { }

        public override async Task<IEnumerable<User>> Get(Expression<Func<User, bool>> filter = null)
        {
            if (filter == null) return await Task.Run(db.Users.AsEnumerable);
            var f = filter.Compile();
            var query = from i in db.Users.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override async Task<User> Get(int id)
        {
            return await db.Users.FindAsync(id);
        }
        public override async Task<User> Create(User input)
        {
            var value = await db.Users.AddAsync(input);
            await db.SaveChangesAsync();
            return value.Entity;
        }
        public override async Task<User> Update(User input)
        {
            var item = await Get(input.Id);
            if (item == null) return null;
            item.BirthDay = input.BirthDay;
            item.Email = input.Email;
            item.FirstName = input.FirstName;
            item.LastName = input.LastName;
            await db.SaveChangesAsync();
            return item;
        }
        public override async Task<bool> Delete(int id)
        {
            var item = await Get(id);
            if (item == null) return false;
            db.Users.Remove(item);
            await db.SaveChangesAsync();
            return true;
        }

    }
}
