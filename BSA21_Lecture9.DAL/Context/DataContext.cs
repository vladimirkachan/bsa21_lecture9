﻿using BSA21_Lecture9.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace BSA21_Lecture9.DAL.Context
{
    public class DataContext : DbContext
    {
        public DbSet<Project> Projects {get; set;}
        public DbSet<Task> Tasks {get; set;}
        public DbSet<Team> Teams {get; set;}
        public DbSet<User> Users {get; set;}

        public DataContext() {}
        public DataContext(DbContextOptions<DataContext> options) : base(options) {} 

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
            @"Server=(LocalDB)\MSSQLLocalDb; Database=ProjectStructureDB9; Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            modelBuilder.Seed();
        }

    }
}
