﻿using System.Linq;
using BSA21_Lecture9.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(e => e.AuthorId).HasColumnName("UserId");
            modelBuilder.Entity<Task>().Property(e => e.PerformerId).HasColumnName("UserId");
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GetTeamsFromBsa();
            var users = GetUsersFromBsa();
            var projects = GetProjectsFromBsa();
            var tasks = GetTasksFromBsa();

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        static Team[] GetTeamsFromBsa()
        {
            var teams = Requests.GetTeams().Result;
            var query = from t in teams select new Team {Id = t.Id + 1, Name = t.Name, CreatedAt = t.CreatedAt};
            return query.ToArray();
        }
        static User[] GetUsersFromBsa()
        {
            var users = Requests.GetUsers().Result;
            var query = from u in users select new User {Id = u.Id + 1, TeamId = u.TeamId + 1, 
                BirthDay = u.BirthDay, Email = u.Email, FirstName = u.FirstName, LastName = u.LastName, RegisteredAt = u.RegisteredAt};
            return query.ToArray();
        }
        static Project[] GetProjectsFromBsa()
        {
            var projects = Requests.GetProjects().Result;
            var query = from p in projects select new Project {Id = p.Id + 1, AuthorId = p.AuthorId + 1, TeamId = p.TeamId + 1, 
                CreatedAt = p.CreatedAt, Deadline = p.Deadline, Description = p.Description, Name = p.Name};
            return query.ToArray();
        }
        static Task[] GetTasksFromBsa()
        {
            var tasks = Requests.GetTasks().Result;
            var query = from t in tasks select new Task {Id = t.Id + 1, PerformerId = t.PerformerId + 1, ProjectId = t.ProjectId + 1, 
                CreatedAt = t.CreatedAt, Description = t.Description, FinishedAt = t.FinishedAt, Name = t.Name, State = t.State};
            return query.ToArray();
        }
    }

}
