﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BSA21_Lecture9.DAL.Entities;
using Newtonsoft.Json;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.DAL
{
    public static class Requests
    {
        static readonly HttpClient client = new();
        const string host = @"https://bsa21.azurewebsites.net/api/";

        static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }
        static async Task<string> GetEndPoint(string url)
        {
            var response = await client.GetAsync(url);
            return await GetResponseBody(response);
        }

        public static async Task<IEnumerable<Project>> GetProjects()
        {
            var response = await client.GetAsync(host + "projects");
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<Task>> GetTasks()
        {
            var response = await client.GetAsync(host + "tasks");
            return JsonConvert.DeserializeObject<IEnumerable<Task>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<Team>> GetTeams()
        {
            var response = await client.GetAsync(host + "teams");
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<User>> GetUsers()
        {
            var response = await client.GetAsync(host + "users");
            return JsonConvert.DeserializeObject<IEnumerable<User>>(await GetResponseBody(response));
        }

        public static async Task<Project> GetProject(int id)
        {
            var response = await client.GetAsync(host + $@"projects/{id}");
            return JsonConvert.DeserializeObject<Project>(await GetResponseBody(response));
        }
        public static async Task<Task> GetTask(int id)
        {
            var response = await client.GetAsync(host + $@"tasks/{id}");
            return JsonConvert.DeserializeObject<Task>(await GetResponseBody(response));
        }
        public static async Task<Team> GetTeam(int id)
        {
            var response = await client.GetAsync(host + $@"teams/{id}");
            return JsonConvert.DeserializeObject<Team>(await GetResponseBody(response));
        }
        public static async Task<User> GetUser(int id)
        {
            var response = await client.GetAsync(host + $@"users/{id}");
            return JsonConvert.DeserializeObject<User>(await GetResponseBody(response));
        }
    }
}
