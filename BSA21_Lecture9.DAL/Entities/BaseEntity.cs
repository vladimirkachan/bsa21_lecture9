﻿namespace BSA21_Lecture9.DAL.Entities
{
    public abstract class BaseEntity 
    {
        public int Id { get; set; }
    }
}
