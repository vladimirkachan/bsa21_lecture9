﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture9.DAL.Entities
{
    public class Project : BaseEntity
    {
        public int AuthorId { get; set; }

        public int TeamId { get; set; }

        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime Deadline { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public User User { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }
    }
}
