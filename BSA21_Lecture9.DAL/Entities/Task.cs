﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture9.DAL.Entities
{
    public class Task : BaseEntity
    {
        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }

        public State State { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? FinishedAt { get; set; }

        [JsonIgnore] public Project Project { get; set; }
        [JsonIgnore] public User User { get; set; }

    }

    public enum State {Zero = 0, One = 1, Two = 2, Three = 3}
}
