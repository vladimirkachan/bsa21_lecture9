﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BSA21_Lecture9.DAL.Entities
{
    public class Team : BaseEntity
    {
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt { get; set; }
    }
}
