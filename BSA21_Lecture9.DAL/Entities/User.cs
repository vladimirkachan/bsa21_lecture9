﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture9.DAL.Entities
{
    public class User : BaseEntity
    {
        public int? TeamId { get; set; }

        [MinLength(2)]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [MinLength(2)]
        [MaxLength(25)]
        public string LastName { get; set; }

        [MinLength(5)]
        [MaxLength(100)]
        public string Email { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime RegisteredAt { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime BirthDay { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }

    }
}
