﻿using System.Windows.Forms;

namespace BSA21_Lecture9.DesktopApp
{
    public partial class TeamForm : Form
    {
        public string TeamName
        {
            get => textBox1.Text;
            set => textBox1.Text = value;
        }

        public TeamForm()
        {
            InitializeComponent();
        }

    }
}
