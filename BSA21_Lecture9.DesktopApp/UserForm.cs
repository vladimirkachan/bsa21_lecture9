﻿using System;
using System.Windows.Forms;
using BSA21_Lecture9.Common.DTO.Team;

namespace BSA21_Lecture9.DesktopApp
{
    public partial class UserForm : Form
    {
        public string FirstName
        {
            get => textBox1.Text;
            set => textBox1.Text = value;
        }

        public string LastName
        {
            get => textBox2.Text;
            set => textBox2.Text = value;
        }

        public string Email
        {
            get => textBox3.Text;
            set => textBox3.Text = value;
        }

        public DateTime BirthDay
        {
            get => dateTimePicker1.Value;
            set => dateTimePicker1.Value = value;
        }

        public TeamDTO Team => (TeamDTO)team_cb.SelectedItem;

        public UserForm()
        {
            InitializeComponent();
        }
        public void Clear()
        {
            textBox1.Text = textBox2.Text = textBox3.Text = "";
            team_cb.SelectedIndex = -1;
        }
    }
}
