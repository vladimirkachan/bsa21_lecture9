﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using BSA21_Lecture9.Common;
using BSA21_Lecture9.Common.DTO.Team;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL;

namespace BSA21_Lecture9.DesktopApp
{
    public partial class Form1 : Form
    {
        readonly TeamForm teamForm = new();
        readonly UserForm userForm = new();

        public Form1()
        {
            InitializeComponent();
            project_dgv.DataSource = project_bs;
            task_dgv.DataSource = task_bs;
            userForm.team_cb.DataSource = team_dgv.DataSource = team_bs;
            user_dgv.DataSource = user_bs;
            AddOwnedForm(teamForm);
            AddOwnedForm(userForm);
        }

        protected override async void OnLoad(EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            await LoadThrowWebAPI();
            userForm.team_cb.DisplayMember = "Name";
            userForm.team_cb.ValueMember = "Id";
            Cursor = Cursors.Default;
        }
        async Task LoadThrowWebAPI()
        {
            project_bs.DataSource = await EndPoints.GetProjects();
            task_bs.DataSource = await EndPoints.GetTasks();
            team_bs.DataSource = await EndPoints.GetTeams();
            user_bs.DataSource = await EndPoints.GetUsers();
        }
        async Task LoadFromBsa()
        {
            project_bs.DataSource = await Requests.GetProjects();
            task_bs.DataSource = await Requests.GetTasks();
            team_bs.DataSource = await Requests.GetTeams();
            user_bs.DataSource = await Requests.GetUsers();
        }
        private async void load1_Click(object sender, EventArgs e)
        {
            await LoadThrowWebAPI();
        }
        private void close1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void teamAdd1_Click(object sender, EventArgs e)
        {
            teamForm.TeamName = "";
            if (teamForm.ShowDialog() != DialogResult.OK) return;
            await EndPoints.AddTeam(new TeamCreateDTO { Name = teamForm.TeamName });
            team_bs.DataSource = await EndPoints.GetTeams();
        }
        private async void teamUpdate1_Click(object sender, EventArgs e)
        {
            var row = team_dgv.CurrentRow;
            if (row == null) return;
            teamForm.TeamName = row.Cells["Name"].Value.ToString();
            if (teamForm.ShowDialog() != DialogResult.OK) return;
            int id = Convert.ToInt32(row.Cells["Id"].Value);
            var dto = new TeamCreateDTO { Name = teamForm.TeamName };
            await EndPoints.UpdateTeam(id, dto);
            team_bs.DataSource = await EndPoints.GetTeams();

        }
        private async void teamDelete1_Click(object sender, EventArgs e)
        {
            var row = team_dgv.CurrentRow;
            if (row == null) return;
            int id = Convert.ToInt32(row.Cells["Id"].Value);
            await EndPoints.DeleteTeam(id);
            team_bs.DataSource = await EndPoints.GetTeams();
        }

        private async void userAdd1_Click(object sender, EventArgs e)
        {
            userForm.Clear();
            if (userForm.ShowDialog() != DialogResult.OK) return;
            var dto = new UserCreateDTO
            {
                FirstName = userForm.FirstName, 
                LastName = userForm.LastName, 
                Email = userForm.Email, 
                BirthDay = userForm.BirthDay
            };
            await EndPoints.AddUser(dto);
            user_bs.DataSource = await EndPoints.GetUsers();
        }

        private void userUpdate1_Click(object sender, EventArgs e)
        {

        }

        private void userDelete1_Click(object sender, EventArgs e)
        {

        }
    }
}
