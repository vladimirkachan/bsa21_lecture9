﻿
namespace BSA21_Lecture9.DesktopApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.project_dgv = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.task_dgv = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.team_dgv = new System.Windows.Forms.DataGridView();
            this.team_ts = new System.Windows.Forms.ToolStrip();
            this.teamAdd1 = new System.Windows.Forms.ToolStripButton();
            this.teamUpdate1 = new System.Windows.Forms.ToolStripButton();
            this.teamDelete1 = new System.Windows.Forms.ToolStripButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.user_dgv = new System.Windows.Forms.DataGridView();
            this.project_bs = new System.Windows.Forms.BindingSource(this.components);
            this.task_bs = new System.Windows.Forms.BindingSource(this.components);
            this.team_bs = new System.Windows.Forms.BindingSource(this.components);
            this.user_bs = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.load1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.close1 = new System.Windows.Forms.ToolStripMenuItem();
            this.user_ts = new System.Windows.Forms.ToolStrip();
            this.userAdd1 = new System.Windows.Forms.ToolStripButton();
            this.userUpdate1 = new System.Windows.Forms.ToolStripButton();
            this.userDelete1 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.project_dgv)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.task_dgv)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.team_dgv)).BeginInit();
            this.team_ts.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.user_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.project_bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.task_bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team_bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_bs)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.user_ts.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.project_dgv);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 398);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Projects";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // project_dgv
            // 
            this.project_dgv.AllowUserToAddRows = false;
            this.project_dgv.AllowUserToDeleteRows = false;
            this.project_dgv.AllowUserToOrderColumns = true;
            this.project_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.project_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.project_dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.project_dgv.Location = new System.Drawing.Point(3, 3);
            this.project_dgv.Name = "project_dgv";
            this.project_dgv.ReadOnly = true;
            this.project_dgv.RowTemplate.Height = 25;
            this.project_dgv.Size = new System.Drawing.Size(786, 392);
            this.project_dgv.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.task_dgv);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 398);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tasks";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // task_dgv
            // 
            this.task_dgv.AllowUserToAddRows = false;
            this.task_dgv.AllowUserToDeleteRows = false;
            this.task_dgv.AllowUserToOrderColumns = true;
            this.task_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.task_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.task_dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.task_dgv.Location = new System.Drawing.Point(3, 3);
            this.task_dgv.Name = "task_dgv";
            this.task_dgv.ReadOnly = true;
            this.task_dgv.RowTemplate.Height = 25;
            this.task_dgv.Size = new System.Drawing.Size(786, 392);
            this.task_dgv.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.team_dgv);
            this.tabPage3.Controls.Add(this.team_ts);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(792, 398);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Teams";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // team_dgv
            // 
            this.team_dgv.AllowUserToAddRows = false;
            this.team_dgv.AllowUserToDeleteRows = false;
            this.team_dgv.AllowUserToOrderColumns = true;
            this.team_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.team_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team_dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_dgv.Location = new System.Drawing.Point(3, 3);
            this.team_dgv.Name = "team_dgv";
            this.team_dgv.ReadOnly = true;
            this.team_dgv.RowTemplate.Height = 25;
            this.team_dgv.Size = new System.Drawing.Size(786, 367);
            this.team_dgv.TabIndex = 0;
            // 
            // team_ts
            // 
            this.team_ts.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.team_ts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teamAdd1,
            this.teamUpdate1,
            this.teamDelete1});
            this.team_ts.Location = new System.Drawing.Point(3, 370);
            this.team_ts.Name = "team_ts";
            this.team_ts.Size = new System.Drawing.Size(786, 25);
            this.team_ts.TabIndex = 1;
            this.team_ts.Text = "toolStrip1";
            // 
            // teamAdd1
            // 
            this.teamAdd1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.teamAdd1.Image = ((System.Drawing.Image)(resources.GetObject("teamAdd1.Image")));
            this.teamAdd1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.teamAdd1.Name = "teamAdd1";
            this.teamAdd1.Size = new System.Drawing.Size(33, 22);
            this.teamAdd1.Text = "Add";
            this.teamAdd1.Click += new System.EventHandler(this.teamAdd1_Click);
            // 
            // teamUpdate1
            // 
            this.teamUpdate1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.teamUpdate1.Image = ((System.Drawing.Image)(resources.GetObject("teamUpdate1.Image")));
            this.teamUpdate1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.teamUpdate1.Name = "teamUpdate1";
            this.teamUpdate1.Size = new System.Drawing.Size(49, 22);
            this.teamUpdate1.Text = "Update";
            this.teamUpdate1.Click += new System.EventHandler(this.teamUpdate1_Click);
            // 
            // teamDelete1
            // 
            this.teamDelete1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.teamDelete1.Image = ((System.Drawing.Image)(resources.GetObject("teamDelete1.Image")));
            this.teamDelete1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.teamDelete1.Name = "teamDelete1";
            this.teamDelete1.Size = new System.Drawing.Size(44, 22);
            this.teamDelete1.Text = "Delete";
            this.teamDelete1.Click += new System.EventHandler(this.teamDelete1_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.user_dgv);
            this.tabPage4.Controls.Add(this.user_ts);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(792, 398);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Users";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // user_dgv
            // 
            this.user_dgv.AllowUserToAddRows = false;
            this.user_dgv.AllowUserToDeleteRows = false;
            this.user_dgv.AllowUserToOrderColumns = true;
            this.user_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.user_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.user_dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_dgv.Location = new System.Drawing.Point(3, 3);
            this.user_dgv.Name = "user_dgv";
            this.user_dgv.ReadOnly = true;
            this.user_dgv.RowTemplate.Height = 25;
            this.user_dgv.Size = new System.Drawing.Size(786, 367);
            this.user_dgv.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.load1,
            this.toolStripSeparator1,
            this.close1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // load1
            // 
            this.load1.Name = "load1";
            this.load1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.load1.Size = new System.Drawing.Size(119, 22);
            this.load1.Text = "&Load";
            this.load1.Click += new System.EventHandler(this.load1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(116, 6);
            // 
            // close1
            // 
            this.close1.Name = "close1";
            this.close1.Size = new System.Drawing.Size(119, 22);
            this.close1.Text = "&Close";
            this.close1.Click += new System.EventHandler(this.close1_Click);
            // 
            // user_ts
            // 
            this.user_ts.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.user_ts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userAdd1,
            this.userUpdate1,
            this.userDelete1});
            this.user_ts.Location = new System.Drawing.Point(3, 370);
            this.user_ts.Name = "user_ts";
            this.user_ts.Size = new System.Drawing.Size(786, 25);
            this.user_ts.TabIndex = 1;
            this.user_ts.Text = "toolStrip1";
            // 
            // userAdd1
            // 
            this.userAdd1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.userAdd1.Image = ((System.Drawing.Image)(resources.GetObject("userAdd1.Image")));
            this.userAdd1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.userAdd1.Name = "userAdd1";
            this.userAdd1.Size = new System.Drawing.Size(33, 22);
            this.userAdd1.Text = "Add";
            this.userAdd1.Click += new System.EventHandler(this.userAdd1_Click);
            // 
            // userUpdate1
            // 
            this.userUpdate1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.userUpdate1.Image = ((System.Drawing.Image)(resources.GetObject("userUpdate1.Image")));
            this.userUpdate1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.userUpdate1.Name = "userUpdate1";
            this.userUpdate1.Size = new System.Drawing.Size(49, 22);
            this.userUpdate1.Text = "Update";
            this.userUpdate1.Click += new System.EventHandler(this.userUpdate1_Click);
            // 
            // userDelete1
            // 
            this.userDelete1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.userDelete1.Image = ((System.Drawing.Image)(resources.GetObject("userDelete1.Image")));
            this.userDelete1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.userDelete1.Name = "userDelete1";
            this.userDelete1.Size = new System.Drawing.Size(44, 22);
            this.userDelete1.Text = "Delete";
            this.userDelete1.Click += new System.EventHandler(this.userDelete1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Project Structure";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.project_dgv)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.task_dgv)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.team_dgv)).EndInit();
            this.team_ts.ResumeLayout(false);
            this.team_ts.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.user_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.project_bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.task_bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team_bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_bs)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.user_ts.ResumeLayout(false);
            this.user_ts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView project_dgv;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource project_bs;
        private System.Windows.Forms.DataGridView task_dgv;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView team_dgv;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView user_dgv;
        private System.Windows.Forms.BindingSource task_bs;
        private System.Windows.Forms.BindingSource team_bs;
        private System.Windows.Forms.BindingSource user_bs;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem load1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem close1;
        private System.Windows.Forms.ToolStrip team_ts;
        private System.Windows.Forms.ToolStripButton teamAdd1;
        private System.Windows.Forms.ToolStripButton teamUpdate1;
        private System.Windows.Forms.ToolStripButton teamDelete1;
        private System.Windows.Forms.ToolStrip user_ts;
        private System.Windows.Forms.ToolStripButton userAdd1;
        private System.Windows.Forms.ToolStripButton userUpdate1;
        private System.Windows.Forms.ToolStripButton userDelete1;
    }
}

