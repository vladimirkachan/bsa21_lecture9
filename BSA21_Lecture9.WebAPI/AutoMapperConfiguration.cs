﻿using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.Common.DTO.Team;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL.Entities;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.WebAPI
{
    public class AutoMapperConfiguration
    {
        public AutoMapper.MapperConfiguration Get()
        {
            var mapperConfiguration = new AutoMapper.MapperConfiguration(c =>
            {
                c.CreateMap<Team, TeamDTO>();
                c.CreateMap<TeamDTO, Team>();

                c.CreateMap<User, UserDTO>();
                c.CreateMap<UserDTO, User>();

                c.CreateMap<Project, ProjectDTO>();
                c.CreateMap<ProjectDTO, Project>();

                c.CreateMap<Task, TaskDTO>();
                c.CreateMap<TaskDTO, Task>();
            });
            return mapperConfiguration;
        }

    }
}
