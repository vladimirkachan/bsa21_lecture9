using BSA21_Lecture9.BLL.Services;
using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.Common.DTO.Team;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL.Context;
using BSA21_Lecture9.DAL.Entities;
using BSA21_Lecture9.DAL.Repositories;
using BSA21_Lecture9.WebAPI.Validators;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BSA21_Lecture9.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<DataContext>(builder =>
                                               builder.UseSqlServer(Configuration.GetConnectionString("ProjectStructureDB9"),
                                                                    options => options.MigrationsAssembly("BSA21_Lecture9.DAL")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IService<Team, TeamDTO>, TeamService>();
            services.AddScoped<IRepository<Team>, TeamRepository>();
            services.AddScoped<IService<User, UserDTO>, UserService>();
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IService<Project, ProjectDTO>, ProjectService>();
            services.AddScoped<IRepository<Project>, ProjectRepository>();
            services.AddScoped<IService<Task, TaskDTO>, TaskService>();
            services.AddScoped<IRepository<Task>, TaskRepository>();

            services.AddSingleton<IValidator<TeamCreateDTO>, TeamValidator>();
            services.AddSingleton<IValidator<UserCreateDTO>, UserValidator>();
            services.AddSingleton<IValidator<ProjectCreateDTO>, ProjectValidator>();
            services.AddSingleton<IValidator<TaskCreateDTO>, TaskValidator>();

            var mapper = new AutoMapperConfiguration();
            services.AddSingleton(_ => mapper.Get().CreateMapper());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BSA21_Lecture9.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BSA21_Lecture9.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
