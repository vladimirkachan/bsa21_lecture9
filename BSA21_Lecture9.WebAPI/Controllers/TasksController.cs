﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using BSA21_Lecture9.BLL.Services;
using BSA21_Lecture9.Common.DTO.Task;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        readonly IService<Task, TaskDTO> taskService;
        readonly IValidator<TaskCreateDTO> validator;

        public TasksController(IService<Task, TaskDTO> taskService,
                               IValidator<TaskCreateDTO> validator)
        {
            this.taskService = taskService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await taskService.Get());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            var item = await taskService.Get(id);
            return item == null ? NotFound($"There is no such task with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] TaskCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            var team = await taskService.Create(dto);
            return CreatedAtAction("POST", team);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TaskDTO>> Put(int id, [FromBody] TaskCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await taskService.Update(dto);
            return updated == null ? NotFound($"No task with ID {id}") : Ok(updated);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await taskService.Delete(id);
            return deleted ? NoContent() : NotFound($"No task with ID {id}");
        }

        [HttpGet("performer/{performerId:int}/{maxLength:int?}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksOnPerformer(int performerId, int maxLength = 45)
        {
            var tasks = await taskService.Get(t => t.PerformerId == performerId && t.Name.Length < maxLength);
            return Ok(tasks);
        }

        [HttpGet("performer/{performerId:int}/year/{year:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetFinishedTask(int performerId, int year)
        {
            var tasks = await taskService.Get(t => t.PerformerId == performerId && t.FinishedAt != null &&
                                        t.FinishedAt >= new DateTime(year, 1, 1) &&
                                        t.FinishedAt <= new DateTime(year, 12, 31));
            return Ok(tasks);
        }

        [HttpGet("projectId/{projectId:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTaskByProject(int projectId)
        {
            return Ok(await taskService.Get(t => t.ProjectId == projectId));
        }

        [HttpGet("unfinished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasks()
        {
            return Ok(await taskService.Get(t => t.FinishedAt == null));
        }

        [HttpGet("finished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetFinishedTasks()
        {
            return Ok(await taskService.Get(t => t.FinishedAt != null));
        }

        [HttpPut("tryFinish")]
        public async Task<ActionResult<int>> TryFinish([FromBody] int id)
        {
            var task = await taskService.Get(id);
            if (task.FinishedAt != null) return Ok(-1);
            task.FinishedAt = DateTime.Now;
            await taskService.Update(task);
            return Ok(id);
        }
    }
}
