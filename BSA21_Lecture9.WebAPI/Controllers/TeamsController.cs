﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture9.BLL.Services;
using BSA21_Lecture9.Common.DTO.Team;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture9.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        readonly IService<Team, TeamDTO> teamService;
        readonly IService<User, UserDTO> userService;
        readonly IValidator<TeamCreateDTO> validator;

        public TeamsController(IService<Team, TeamDTO> teamService, IService<User, UserDTO> userService, IValidator<TeamCreateDTO> validator)
        {
            this.teamService = teamService;
            this.userService = userService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await teamService.Get());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            var item = await teamService.Get(id);
            return item == null ? NotFound($"There is no such team with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] TeamCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            var team = await teamService.Create(dto);
            return CreatedAtAction("POST", team);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Put(int id, [FromBody] TeamCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await teamService.Update(dto);
            //return updated ? Ok(await teamService.Get(id)) : NotFound($"No team with ID {id}");
            return updated == null ? NotFound($"No team with ID {id}") : Ok(updated);
        }
        
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => teamService.Delete(id));
            return deleted ? NoContent() : NotFound($"No team with ID {id}");
        }

        [HttpGet("withUsers/{minAge:int}/{maxAge:int}")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO,IEnumerable<UserDTO>>>>> GetTeamsWithUsers(int minAge = 10, int maxAge = 80)
        {
            var teams = await teamService.Get();
            var query = from team in teams
                        orderby team.CreatedAt descending
                        select KeyValuePair.Create(team,
                            from user in userService.Get(u => u.TeamId == team.Id &&
                                                         DateTime.Now.Year - u.BirthDay.Year >= minAge &&
                                                         DateTime.Now.Year - u.BirthDay.Year <= maxAge).Result
                            orderby user.RegisteredAt descending
                            select user);
            return Ok(query.AsEnumerable());
        }

        [HttpGet("andUsers")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>>> GetTeamsWithUsers()
        {
            var teams = await teamService.Get();
            var query = from t in teams
                        select KeyValuePair.Create(t, from u in userService.Get(ue => ue.TeamId == t.Id).Result select u);
            return Ok(query.AsEnumerable());
        }
    }
}
