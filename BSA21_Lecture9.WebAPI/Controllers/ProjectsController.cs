﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture9.BLL.Services;
using BSA21_Lecture9.Common.DTO.Custom;
using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        readonly IService<Project, ProjectDTO> projectService;
        readonly IService<Task, TaskDTO> taskService;
        readonly IService<User, UserDTO> userService;
        readonly IValidator<ProjectCreateDTO> validator;

        public ProjectsController(IService<Project, ProjectDTO> projectService,
                                  IService<Task, TaskDTO> taskService,
                                  IService<User, UserDTO> userService,
                                  IValidator<ProjectCreateDTO> validator)
        {
            this.projectService = projectService;
            this.taskService = taskService;
            this.userService = userService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await projectService.Get());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            var item = await projectService.Get(id);
            return item == null ? NotFound($"There is no such project with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            var value = await projectService.Create(dto);
            return CreatedAtAction("POST", value);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserDTO>> Put(int id, [FromBody] ProjectCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await projectService.Update(dto);
            return updated == null ? NotFound($"No user with ID {id}") : Ok(updated);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await projectService.Delete(id);
            return deleted ? NoContent() : NotFound($"No project with ID {id}");
        }

        [HttpGet("{projectId:int}/taskCount")]
        public async Task<ActionResult<int>> TaskCount(int projectId)
        {
            var tasks = await taskService.Get(a => a.ProjectId == projectId);
            return Ok(tasks.Count());
        }

        [HttpGet("withTasks")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<ProjectDTO, IEnumerable<TaskDTO>>>>> GetProjectsWithTasks()
        {
            var projects = await projectService.Get();
            var query = from p in projects
                        select KeyValuePair.Create(p,
                                                   from t in taskService.Get(te => te.ProjectId == p.Id).Result
                                                   select t);
            return Ok(query.AsEnumerable());
        }

        [HttpGet("custom/{projectId:int}")]
        public async Task<ActionResult<CustomProjectDTO>> GetCustomProject(int projectId)
        {
            var project = await projectService.Get(projectId);
            var tasks = (await taskService.Get(te => te.ProjectId == projectId)).ToList();
            var query1 = from t in tasks orderby t.Description.Length select t;
            var query2 = from t in tasks orderby t.Name.Length select t;
            int? teamId = (await projectService.GetEntity(projectId)).TeamId;
            var users = await userService.Get(ue => ue.TeamId == teamId);
            var customProject = new CustomProjectDTO
            {
                Project = project,
                LongestDescriptionTask = await System.Threading.Tasks.Task.Run(query1.LastOrDefault),
                ShortestNameTask = await System.Threading.Tasks.Task.Run(query2.FirstOrDefault),
                TotalCountOfTeamUsers = await System.Threading.Tasks.Task.Run(users.Count)
            };
            return Ok(customProject);
        }
    }
}
