﻿using BSA21_Lecture9.Common.DTO.Team;
using FluentValidation;

namespace BSA21_Lecture9.WebAPI.Validators
{
    public class TeamValidator : AbstractValidator<TeamCreateDTO> 
    {
        public TeamValidator()
        {
            RuleFor(i => i.Name).NotEmpty().Length(2, 100).WithMessage("Team name length must be 2-100");
        }
    }
}
