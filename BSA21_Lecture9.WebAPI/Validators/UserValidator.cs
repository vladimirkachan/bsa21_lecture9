﻿using BSA21_Lecture9.Common.DTO.User;
using FluentValidation;

namespace BSA21_Lecture9.WebAPI.Validators
{
    public class UserValidator : AbstractValidator<UserCreateDTO>
    {
        public UserValidator()
        {
            RuleFor(i => i.FirstName).NotEmpty().Length(2, 20).WithMessage("User first name length must be 2-20");
            RuleFor(i => i.LastName).NotEmpty().Length(2, 25).WithMessage("User last name length must be 2-25");
            RuleFor(i => i.Email).NotEmpty().Length(2, 25).WithMessage("User email length must be 5-100");
        }
    }
}
