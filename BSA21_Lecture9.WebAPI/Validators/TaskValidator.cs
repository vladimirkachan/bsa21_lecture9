﻿using BSA21_Lecture9.Common.DTO.Task;
using FluentValidation;

namespace BSA21_Lecture9.WebAPI.Validators
{
    public class TaskValidator : AbstractValidator<TaskCreateDTO>
    {
        public TaskValidator()
        {
            RuleFor(i => i.Name).NotEmpty().Length(2, 100).WithMessage("Task name length must be 2-100");
            RuleFor(i => i.Description).Length(0, 200).WithMessage("Task description length must be 0-200");
        }
    }
}
