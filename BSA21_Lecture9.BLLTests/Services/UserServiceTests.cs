﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BSA21_Lecture9.Common.DTO.User;
using BSA21_Lecture9.DAL.Entities;
using BSA21_Lecture9.DAL.Repositories;
using BSA21_Lecture9.WebAPI;
using FakeItEasy;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace BSA21_Lecture9.BLL.Services.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        readonly IList<User> users = new List<User>();
        readonly IService<User, UserDTO> userService;
        readonly IMapper mapper = new AutoMapperConfiguration().Get().CreateMapper();
        readonly IRepository<User> fakeRepository = A.Fake<IRepository<User>>();
        readonly IUnitOfWork fakeUnit = A.Fake<IUnitOfWork>();

        public UserServiceTests()
        {
            userService = new UserService(fakeRepository, mapper);
        }

        [OneTimeSetUp]
        public void TestSetup()
        {
            A.CallTo(() => fakeRepository.Create(A<User>._)).Invokes((User u) => { u.Id = users.Count + 1; users.Add(u); });
            A.CallTo(() => fakeUnit.Users).Returns(fakeRepository);
        }

        [Test]
        public void CreateUserTestWhenCorrectData()
        {
            var userDto = new UserCreateDTO { FirstName = "Maria", LastName = "Sokolova", Email = "mariasokolova@gmail.com", 
                BirthDay = new DateTime(2000, 1, 1), RegisteredAt = DateTime.Now };
            var excepted = users.Count + 1;

            userService.Create(userDto);

            Assert.That(users.Count == excepted);
        }

        [Test]
        public void CreateUserTestWhenFirstNameEmpty()
        {
            var userDto = new UserCreateDTO { FirstName = "", LastName = "Sokolova", Email = "mariasokolova@gmail.com", BirthDay = new DateTime(2000, 1, 1), RegisteredAt = DateTime.Now };
            var excepted = users.Count;

            userService.Create(userDto);

            Assert.That(users.Count == excepted);
        }
    }
}