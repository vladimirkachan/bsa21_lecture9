﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture9.Common.DTO.Task;
using BSA21_Lecture9.DAL.Repositories;
using Task = BSA21_Lecture9.DAL.Entities.Task;

namespace BSA21_Lecture9.BLL.Services
{
    public class TaskService : IService<Task, TaskDTO>
    {
        readonly IRepository<Task> repository;
        readonly IMapper mapper;

        public TaskService(IRepository<Task> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> Get(Expression<Func<Task, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<TaskDTO>>(await repository.Get(filter));
        }
        public async Task<TaskDTO> Get(int id)
        {
            var entity = await repository.Get(id);
            return entity == null ? null : mapper.Map<TaskDTO>(entity);
        }
        public async Task<TaskDTO> Create(TaskDTO dto)
        {
            var entity = mapper.Map<Task>(dto);
            var task = await repository.Create(entity);
            return mapper.Map<TaskDTO>(task);
        }
        public async Task<TaskDTO> Update(TaskDTO dto)
        {
            return mapper.Map<TaskDTO>(await repository.Update(mapper.Map<Task>(dto)));
        }
        public async Task<bool> Delete(int id)
        {
            return await repository.Delete(id);
        }
        public async Task<Task> GetEntity(int id)
        {
            return await repository.Get(id);
        }
    }
}
