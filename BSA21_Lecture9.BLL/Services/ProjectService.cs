﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture9.Common.DTO.Project;
using BSA21_Lecture9.DAL.Entities;
using BSA21_Lecture9.DAL.Repositories;

namespace BSA21_Lecture9.BLL.Services
{
    public class ProjectService : IService<Project, ProjectDTO>
    {
        readonly IRepository<Project> repository;
        readonly IMapper mapper;

        public ProjectService(IRepository<Project> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDTO>> Get(Expression<Func<Project, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<ProjectDTO>>(await repository.Get(filter));
        }
        public async Task<ProjectDTO> Get(int id)
        {
            var entity = await repository.Get(id);
            return entity == null ? null : mapper.Map<ProjectDTO>(entity);
        }
        public async Task<ProjectDTO> Create(ProjectDTO dto)
        {
            var entity = mapper.Map<Project>(dto);
            var project = await repository.Create(entity);
            return mapper.Map<ProjectDTO>(project);
        }
        public async Task<ProjectDTO> Update(ProjectDTO dto)
        {
            return mapper.Map<ProjectDTO>(await repository.Update(mapper.Map<Project>(dto)));
        }
        public async Task<bool> Delete(int id)
        {
            return await repository.Delete(id);
        }
        public async Task<Project> GetEntity(int id)
        {
            return await repository.Get(id);
        }
    }
}
