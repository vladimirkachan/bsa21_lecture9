﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture9.Common;
using BSA21_Lecture9.Common.DTO.Custom;
using BSA21_Lecture9.Common.DTO.Task;
using Timer = System.Timers.Timer;

namespace BSA21_Lecture9.ConsoleClient
{
    public class Client
    {
        static readonly Random random = new();

         async Task<int> MarkedTaskHandler()
         {
             var tasks = (await EndPoints.GetTasks()).ToList();
             var t = tasks[random.Next(tasks.Count)];
             Console.WriteLine($"Selected: {t.Id}. {t.Name} {t.CreatedAt:d}-{t.FinishedAt:d}");
             var id = await EndPoints.TryFinishTask(t.Id);
             return id;
         }
         public async Task<int> TryFinishTask(int id)
        {
            var result = await EndPoints.TryFinishTask(id);
            return result;
        }

        public async  Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int> completionSource = new();
            Timer timer = new(){Interval = delay};
            timer.Start();
            var id = await MarkedTaskHandler();
            timer.Elapsed += (_, _) =>
            {
                if (id == -1) completionSource.SetException(new Exception("This task has already been completed"));
                else completionSource.SetResult(id);
            };
            return await completionSource.Task;
        }

        #region Lecture 1 - 5
        public async Task DemoProjects()
        {
            var projects = await EndPoints.GetProjects();
            foreach (var p in projects)
                Console.WriteLine($"\t{p.Id}. {p.Name} created at {p.CreatedAt:d}, Deadline: {p.Deadline:d}\n{p.Description}\n");
        }
        public async Task<IList<TaskDTO>> DemoTasks()
        {
            var tasks = (await EndPoints.GetTasks()).ToList();
            foreach (var t in tasks)
                Console.WriteLine($"\t{t.Id}. {t.Name}, {t.CreatedAt:d} - {t.FinishedAt:d}\n {t.Description}\n");
            return tasks;
        }
        public async Task DemoUsers()
        {
            var users = await EndPoints.GetUsers();
            foreach (var u in users)
                Console.WriteLine($"{u.Id}. {u.LastName} {u.FirstName}, {u.Email}, {u.BirthDay:d}, registered at {u.RegisteredAt}");
        }
        public async Task DemoTeams()
        {
            var teams = await EndPoints.GetTeams();
            foreach (var t in teams) Console.WriteLine($"{t.Id}. {t.Name} {t.CreatedAt:d}");
        }
        public async Task DemoTaskCount()
        {
            var projects = (await EndPoints.GetProjects()).ToList();
            foreach(var p in projects)
                Console.WriteLine($"{p.Id}. {p.Name} - TaskCount {await EndPoints.TaskCount(p.Id)}");
        }
        public async Task DemoTeamsAndUsers()
        {
            var pairs = await EndPoints.GetTeamsAndUsers();
            foreach (var (team, users) in pairs)
            {
                Console.WriteLine($"\t{team.Id}. {team.Name} count = {users.Count()}");
                foreach (var u in users)
                    Console.WriteLine($"{u.Id} {u.FirstName} {u.LastName}, Age = {DateTime.Now.Year - u.BirthDay.Year}");
                Console.WriteLine();
            }
        }
        public async Task DemoTasksOnPerformer(int performerId, int maxLength = 45)
        {
            var tasks = await EndPoints.GetTasksOnPerformer(performerId, maxLength);
            Console.WriteLine($"\tcount = {tasks.Count()}");
            foreach (var t in tasks)
                Console.WriteLine($"{t.Id}. {t.Name}, finished at {t.FinishedAt:d}");
        }
        public async Task DemoFinishedTask(int performerId, int year)
        {
            var task = await EndPoints.GetFinishedTask(performerId, year);
            Console.WriteLine($"Count of selected tasks = {task.Count()}");
            foreach (var t in task)
                Console.WriteLine($"{t.Id}. {t.Name} finished at {t.FinishedAt:d}");
        }
        public async Task DemoTeamsWithUsers(int minAge = 10, int maxAge = 80)
        {
            var pairs = await EndPoints.GetTeamUsers(minAge, maxAge);
            foreach (var (team, users) in pairs)
            {
                Console.WriteLine($"\t{team.Id}. {team.Name} count = {users.Count()}");
                foreach (var u in users)
                    Console.WriteLine($"{u.Id} {u.FirstName} {u.LastName}, Age = {DateTime.Now.Year - u.BirthDay.Year}");
                Console.WriteLine();
            }
        }
        public async Task DemoProjectsWithTasks()
        {
            var pairs = await EndPoints.GetProjectsWithTasks();
            foreach (var (project, tasks) in pairs)
            {
                Console.WriteLine($"\t{project.Id}. {project.Name} count of tasks = {tasks.Count()}");
                foreach (var t in tasks)
                    Console.WriteLine($"{t.Id}. {t.Name}");
            }
        }
        public async Task DemoTasksByProject(int projectId)
        {
            var project = await EndPoints.GetProject(projectId);
            var tasks = await EndPoints.GetTasksByProject(projectId);
            Console.WriteLine($"\tCount = {tasks.Count()} by project '{project.Id}. {project.Name}'");
            foreach (var t in tasks)
                Console.WriteLine($"{t.Id}. {t.Name} finished at {t.FinishedAt}");
            Console.WriteLine();
        }
        public async Task DemoSortedUsers()
        {
            var pairs = await EndPoints.GetSortedUsers();
            foreach (var (user, tasks) in pairs)
            {
                Console.WriteLine($"\t{user.Id}. {user.FirstName} {user.LastName}, count of tasks: {tasks.Count()}\n");
                foreach (var t in tasks)
                    Console.WriteLine($"{t.Id:000}. {t.Name}");
                Console.WriteLine();
            }
        }
        public async Task DemoCustomUser(int userId)
        {
            var customUser = await EndPoints.GetUserObject(userId);
            var user = customUser.User;
            Console.WriteLine($"User - \n{user.Id}. {user.LastName} {user.FirstName}, {user.BirthDay:d} {user.Email}, registered at {user.RegisteredAt:d}\n");
            var p = customUser.LastProject;
            Console.WriteLine($"Last project - \n{p.Id}. {p.Name}, created {p.CreatedAt:d}, deadline {p.Deadline:d}\n{p.Description}\n");
            Console.WriteLine($"TotalTaskUnderLastProject = {customUser.TotalTaskUnderLastProject}");
            Console.WriteLine($"TotalUnperformedTask = {customUser.TotalUnperformedTask}\n");
            var t = customUser.LongestTask;
            Console.WriteLine($"Longest task -\n{t.Id}. {t.Name}, created at {t.CreatedAt:d}, finished at {t.FinishedAt:d}\n{t.Description}");
        }
        public async Task DemoCustomProject(int projectId)
        {
            var customProject = await EndPoints.GetCustomProject(projectId);
            PrintCustomProject(customProject);
        }
        static void PrintCustomProject(CustomProjectDTO customProject)
        {
            var p = customProject.Project;
            var t1 = customProject.LongestDescriptionTask;
            var t2 = customProject.ShortestNameTask;
            Console.WriteLine(
            $"Project -\n{p.Id}. {p.Name}, created at {p.CreatedAt:d}, deadline {p.Deadline:d}\n{p.Description}\n");
            Console.WriteLine(
            $"Longest description task -\n{t1.Id}. {t1.Name}, created at {t1.CreatedAt:d}, finished at {t1.FinishedAt:d}\n{t1.Description}\n");
            Console.WriteLine(
            $"Shortest name task -\n{t2.Id}. {t2.Name}, created at {t2.CreatedAt:d}, finished at {t2.FinishedAt:d}\n{t2.Description}\n");
            Console.WriteLine($"Total count of team users = {customProject.TotalCountOfTeamUsers}");
        }
        public async Task DemoUnfinishedTasks()
        {
            var tasks = (await EndPoints.GetUnfinishedTasks()).ToList();
            Console.WriteLine($"\tCount of unfinished tasks: {tasks.Count}");
            foreach (var t in tasks)
                Console.WriteLine($"{t.Id}. {t.Name}, created at {t.CreatedAt:d}, finished at {t.FinishedAt:d}");
        }
        public async Task DemoFinishedTasks()
        {
            var tasks = (await EndPoints.GetFinishedTasks()).ToList();
            Console.WriteLine($"\tCount of finished tasks: {tasks.Count}");
            foreach (var t in tasks)
                Console.WriteLine($"{t.Id}. {t.Name}, created at {t.CreatedAt:d}, finished at {t.FinishedAt:d}");
        }
        #endregion


    }
}
