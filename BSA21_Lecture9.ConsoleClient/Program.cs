﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using Timer = System.Timers.Timer;

namespace BSA21_Lecture9.ConsoleClient
{
    class Program
    {
        static async Task Main()
        {
            START_TASKS:
            CancellationTokenSource cancellation1 = new(), cancellation2 = new();
            var token1 = cancellation1.Token;
            var token2 = cancellation2.Token;
            var task1 = Task.Factory.StartNew(MarkedMethod, token1);
            var task2 = Task.Factory.StartNew(InfoMethod, token2);

            void CancelTasks()
            {
                cancellation1.Cancel();
                cancellation2.Cancel();
                Task.WaitAll(task1, task2);
            }

            Client client = new();
            while (true)
            {
                var cki = Console.ReadKey();
                char c = cki.KeyChar;
                switch (c)
                {
                    default:
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        Console.WriteLine("Enter key:");
                        Console.WriteLine("1 - Show all projects, tasks, teams and users");
                        Console.WriteLine("2 - Show list of tasks assigned to a concrete performer");
                        Console.WriteLine("3 - Show list of tasks that are finished in 2021 for a user with id 100");
                        Console.WriteLine("4 - Show list of sorted teams and users");
                        Console.WriteLine("5 - Show list of users with sorted tasks");
                        Console.WriteLine("6 - Show custom user structure with id 30");
                        Console.WriteLine("7 - Show custom project structure with id 96");
                        Console.WriteLine("8 - Show all projects, tasks, teams and users");
                        Console.WriteLine("9 - Show all projects, tasks, teams and users");
                        break;
                    case 'q':
                    case 'Q':
                    case 'й':
                    case 'Й':
                        CancelTasks();
                        return;
                    case 'n':
                    case 'N':
                    case 'т':
                    case 'Т':
                        Console.Clear();
                        goto START_TASKS;
                    case '1':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoTaskCount();
                        ShowMenuInfo();
                        break;
                    case '2':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoTasksOnPerformer(22, 100);
                        ShowMenuInfo();
                        break;
                    case '3':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoFinishedTask(100, 2021);
                        ShowMenuInfo();
                        break;
                    case '4':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoTeamsWithUsers();
                        ShowMenuInfo();
                        break;
                    case '5':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoSortedUsers();
                        ShowMenuInfo();
                        break;
                    case '6':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoCustomUser(30);
                        ShowMenuInfo();
                        break;
                    case '7':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoCustomProject(96);
                        ShowMenuInfo();
                        break;
                    case '8':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoUnfinishedTasks();
                        Console.WriteLine("\n----------------------------------\n");
                        await client.DemoFinishedTasks();
                        ShowMenuInfo();
                        break;
                    case '9':
                        CancelTasks();
                        await Task.Delay(1000);
                        Console.Clear();
                        await client.DemoProjects();
                        Console.WriteLine("\n----------------------------------\n");
                        await client.DemoTasks();
                        Console.WriteLine("\n----------------------------------\n");
                        await client.DemoTeams();
                        Console.WriteLine("\n----------------------------------\n");
                        await client.DemoUsers();
                        ShowMenuInfo();
                        break;
                }

            }
        }
        static void ShowMenuInfo()
        {
            "\nEnter:\n'Q' - to exit\nPress any key - to go to the menu\n'N' - to start marking finished tasks\n".PrintLine(
            ConsoleColor.Blue);
        }
        static Task InfoMethod(object obj)
        {
            CancellationToken token = (CancellationToken)obj;
            token.ThrowIfCancellationRequested();
            TaskCompletionSource completionSource = new();
            Timer timer = new Timer { Interval = 8000, Enabled = true };
            timer.Elapsed += (_, _) =>
            {
                if (token.IsCancellationRequested) token.ThrowIfCancellationRequested();
                ShowMenuInfo();
            };
            return completionSource.Task;
        }
        static async Task MarkedMethod(object obj)
        {
            CancellationToken token = (CancellationToken)obj;
            token.ThrowIfCancellationRequested();
            Client client = new();
            for (; ; )
            {
                if (token.IsCancellationRequested) token.ThrowIfCancellationRequested();
                try
                {
                    int id = await client.MarkRandomTaskWithDelay(1000);
                    $"Id = {id}".PrintLine(ConsoleColor.Green);
                }
                catch (Exception ex)
                {
                    ex.Message.PrintLine(ConsoleColor.Red);
                }
            }
        }
    }
}
